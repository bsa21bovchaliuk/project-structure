﻿using Client.Enums;
using Client.Interfaces;
using Client.Models.SimpifiedModels;
using Client.Services;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client
{
    public class Facade
    {
        private readonly IProjectService projectService;
        private readonly ITaskService taskService;
        private readonly IUserService userService;
        private readonly ITeamService teamService;

        public Facade()
        {
            var httpclient = new HttpClient();
            projectService = new ProjectService(httpclient);
            taskService = new TaskService(httpclient);
            userService = new UserService(httpclient);
            teamService = new TeamService(httpclient);
        }

        public async Task Start()
        {
            while (true)
            {
                Console.WriteLine("Choose command:");
                Console.WriteLine("1 - print tasks amount in specific user's projects");
                Console.WriteLine($"2 - print task list performed by user, where task name < {Constants.MaxNameLength} characters");
                Console.WriteLine($"3 - print task list finished in currect({DateTime.Now.Year})");
                Console.WriteLine($"4 - print users older {Constants.MinAge} years, grouped by teams");
                Console.WriteLine("5 - print users sorted by first_name with tasks sorted by name length descending");
                Console.WriteLine("6 - print user's info");
                Console.WriteLine("7 - print projects info");
                Console.WriteLine("8 - create project");
                Console.WriteLine("9 - create user");
                Console.WriteLine("10 - create task");
                Console.WriteLine("11 - create team");
                Console.WriteLine("12 - update project");
                Console.WriteLine("13 - update user");
                Console.WriteLine("14 - update task");
                Console.WriteLine("15 - update team");
                Console.WriteLine("16 - delete project");
                Console.WriteLine("17 - delete user");
                Console.WriteLine("18 - delete task");
                Console.WriteLine("19 - delete team");

                Console.WriteLine("0 - exit");
                var commandString = Console.ReadLine();
                Console.WriteLine();
                try
                {
                    int command = int.Parse(commandString);

                    switch (command)
                    {
                        case 0:
                            return;
                        case 1:
                            await PrintTaskCountInProjectsByUser();
                            break;
                        case 2:
                            await PrintTasksPerformedByUser();
                            break;
                        case 3:
                            await PrintFinishedTasksByUser();
                            break;
                        case 4:
                            await PrintUsersGroupedByTeam();
                            break;
                        case 5:
                            await PrintTasksGroupedByUser();
                            break;
                        case 6:
                            await PrintUserInfo();
                            break;
                        case 7:
                            await PrintProjectsInfo();
                            break;
                        case 8:
                            await CreateProjectAsync();
                            break;
                        case 9:
                            await CreateUserAsync();
                            break;
                        case 10:
                            await CreateTaskAsync();
                            break;
                        case 11:
                            await CreateTeamAsync();
                            break;
                        case 12:
                            await UpdateProjectAsync();
                            break;
                        case 13:
                            await UpdateUserAsync();
                            break;
                        case 14:
                            await UpdateTaskAsync();
                            break;
                        case 15:
                            await UpdateTeamAsync();
                            break;
                        case 16:
                            await DeleteProjectAsync();
                            break;
                        case 17:
                            await DeleteUserAsync();
                            break;
                        case 18:
                            await DeleteTaskAsync();
                            break;
                        case 19:
                            await DeleteTeamAsync();
                            break;
                    }
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                Console.WriteLine();
            }
        }

        public int ReadInt()
        {
            var idString = Console.ReadLine();
            return int.Parse(idString);
        }

        public DateTime ReadDate()
        {
            var dateString = Console.ReadLine();
            return Convert.ToDateTime(dateString);
        }

        public TaskState ReadState()
        {
            var stateString = Console.ReadLine();
            var state = int.Parse(stateString);

            if (state < (int)TaskState.Created || state > (int)TaskState.Canceled)
                throw new FormatException($"Unknown State \"{state}\"!");
            return (TaskState)state;
        }

        public async Task PrintTaskCountInProjectsByUser()
        {
            Console.WriteLine("Enter user id:");
            var userId = ReadInt();

            Console.WriteLine(await projectService.GetProjects_TaskCountByUser(userId));
        }

        public async Task PrintTasksPerformedByUser()
        {
            Console.WriteLine("Enter user id:");
            var userId = ReadInt();


            foreach (var task in (await taskService.GetTasksPerformedByUser(userId)).ToList())
            {
                Console.WriteLine(task);
            }
        }

        public async Task PrintFinishedTasksByUser()
        {
            Console.WriteLine("Enter user id:");
            var userId = ReadInt();

            foreach (var task in (await taskService.GetFinishedTasksByUser(userId)).ToList())
            {
                Console.WriteLine(task);
            }
        }

        public async Task PrintUsersGroupedByTeam()
        {

            foreach (var item in (await userService.GetUsersGroupedByTeam()).ToList())
            {
                Console.WriteLine(item);
            }
        }

        public async Task PrintTasksGroupedByUser()
        {

            foreach (var item in (await taskService.GetTasksGroupedByUser()).ToList())
            {
                Console.Write(item);
            }
        }

        public async Task PrintUserInfo()
        {
            Console.WriteLine("Enter user id:");
            var userId = ReadInt();

            var userInfo = await userService.GetUserInfo(userId);

            Console.Write(userInfo);
        }

        public async Task PrintProjectsInfo()
        {

            foreach (var projectInfo in (await projectService.GetProjectsInfo()).ToList())
            {
                Console.WriteLine(projectInfo);
            }
        }

        public async Task DeleteProjectAsync()
        {
            Console.WriteLine("Enter project id:");
            var id = ReadInt();

            await projectService.DeleteProjectByIdAsync(id);
            Console.WriteLine("Project Deleted!");
        }

        public async Task DeleteUserAsync()
        {
            Console.WriteLine("Enter user id:");
            var id = ReadInt();

            await userService.DeleteUserByIdAsync(id);
            Console.WriteLine("User Deleted!");
        }

        public async Task DeleteTeamAsync()
        {
            Console.WriteLine("Enter team id:");
            var id = ReadInt();

            await teamService.DeleteTeamByIdAsync(id);
            Console.WriteLine("Team Deleted!");
        }

        public async Task DeleteTaskAsync()
        {
            Console.WriteLine("Enter task id:");
            var id = ReadInt();

            await taskService.DeleteTaskByIdAsync(id);
            Console.WriteLine("Task Deleted!");
        }

        public async Task CreateProjectAsync()
        {
            Console.WriteLine("Enter AuthotId:");
            var authorId = ReadInt();
            Console.WriteLine("Enter TeamId:");
            var teamId = ReadInt();
            Console.WriteLine("Enter name:");
            var name = Console.ReadLine();
            Console.WriteLine("Enter description:");
            var description = Console.ReadLine();
            Console.WriteLine("Enter deadline in format dd/mm/yyyy:");
            var deadline = ReadDate();

            Console.WriteLine(await projectService.CreateProjectAsync(new ProjectCreateModel
            {
                AuthorId = authorId,
                TeamId = teamId,
                Name = name,
                Description = description,
                Deadline = deadline
            }));
        }

        public async Task CreateUserAsync()
        {
            Console.WriteLine("Enter TeamId:");
            var teamId = ReadInt();
            Console.WriteLine("Enter FirstName:");
            var fname = Console.ReadLine();
            Console.WriteLine("Enter LastName:");
            var lname = Console.ReadLine();
            Console.WriteLine("Enter Email:");
            var email = Console.ReadLine();
            Console.WriteLine("Enter birthdate in format dd/mm/yyyy:");
            var birthday = ReadDate();

            Console.WriteLine(await userService.CreateUserAsync(new UserCreateModel
            {
                TeamId = teamId,
                FirstName = fname,
                LastName = lname,
                Email = email,
                Birthday = birthday
            }));
        }

        public async Task CreateTeamAsync()
        {
            Console.WriteLine("Enter Name:");
            var name = Console.ReadLine();

            Console.WriteLine(await teamService.CreateTeamAsync(new TeamCreateModel
            {
                Name = name
            }));
        }

        public async Task CreateTaskAsync()
        {
            Console.WriteLine("Enter ProjectId:");
            var projectId = ReadInt();
            Console.WriteLine("Enter PerformerId:");
            var performerId = ReadInt();
            Console.WriteLine("Enter name:");
            var name = Console.ReadLine();
            Console.WriteLine("Enter description:");
            var description = Console.ReadLine();
            Console.WriteLine("Enter FinishedAt in format dd/mm/yyyy:");
            var finished = ReadDate();
            Console.WriteLine("Enter taskState 0 - Created 1 - Started 2 - Finished 3 - Canceled");
            var state = ReadState();

            Console.WriteLine(await taskService.CreateTaskAsync(new TaskCreateModel
            {
                ProjectId = projectId,
                PerformerId = performerId,
                Name = name,
                Description = description,
                FinishedAt = finished,
                State = state
            }));
        }

        public async Task UpdateProjectAsync()
        {
            Console.WriteLine("Enter Id:");
            var id = ReadInt();
            Console.WriteLine("Enter AuthotId:");
            var authorId = ReadInt();
            Console.WriteLine("Enter TeamId:");
            var teamId = ReadInt();
            Console.WriteLine("Enter name:");
            var name = Console.ReadLine();
            Console.WriteLine("Enter description:");
            var description = Console.ReadLine();
            Console.WriteLine("Enter deadline in format dd/mm/yyyy:");
            var deadline = ReadDate();

            Console.WriteLine(await projectService.UpdateProjectAsync(new ProjectUpdateModel
            {
                Id = id,
                AuthorId = authorId,
                TeamId = teamId,
                Name = name,
                Description = description,
                Deadline = deadline
            }));
        }

        public async Task UpdateUserAsync()
        {
            Console.WriteLine("Enter Id:");
            var id = ReadInt();
            Console.WriteLine("Enter TeamId:");
            var teamId = ReadInt();
            Console.WriteLine("Enter FirstName:");
            var fname = Console.ReadLine();
            Console.WriteLine("Enter LastName:");
            var lname = Console.ReadLine();
            Console.WriteLine("Enter Email:");
            var email = Console.ReadLine();
            Console.WriteLine("Enter birthdate in format dd/mm/yyyy:");
            var birthday = ReadDate();

            Console.WriteLine(await userService.UpdateUserAsync(new UserUpdateModel
            {
                Id = id,
                TeamId = teamId,
                FirstName = fname,
                LastName = lname,
                Email = email,
                Birthday = birthday
            }));
        }

        public async Task UpdateTeamAsync()
        {
            Console.WriteLine("Enter Id:");
            var id = ReadInt();
            Console.WriteLine("Enter Name:");
            var name = Console.ReadLine();

            Console.WriteLine(await teamService.UpdateTeamAsync(new TeamUpdateModel
            {
                Id = id,
                Name = name
            }));
        }

        public async Task UpdateTaskAsync()
        {
            Console.WriteLine("Enter Id:");
            var id = ReadInt();
            Console.WriteLine("Enter ProjectId:");
            var projectId = ReadInt();
            Console.WriteLine("Enter PerformerId:");
            var performerId = ReadInt();
            Console.WriteLine("Enter name:");
            var name = Console.ReadLine();
            Console.WriteLine("Enter description:");
            var description = Console.ReadLine();
            Console.WriteLine("Enter FinishedAt in format dd/mm/yyyy:");
            var finished = ReadDate();
            Console.WriteLine("Enter taskState 0 - Created 1 - Started 2 - Finished 3 - Canceled");
            var state = ReadState();

            Console.WriteLine(await taskService.UpdateTaskAsync(new TaskUpdateModel
            {
                Id = id,
                ProjectId = projectId,
                PerformerId = performerId,
                Name = name,
                Description = description,
                FinishedAt = finished,
                State = state
            }));
        }
    }
}