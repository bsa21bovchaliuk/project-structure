﻿using System;

namespace Client.Models.SimpifiedModels
{
    public class ProjectCreateModel
    {
#nullable enable
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
    }
}
