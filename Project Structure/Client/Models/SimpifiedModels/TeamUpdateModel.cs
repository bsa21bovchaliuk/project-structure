﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Models.SimpifiedModels
{
    public class TeamUpdateModel
    {
#nullable enable
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}
