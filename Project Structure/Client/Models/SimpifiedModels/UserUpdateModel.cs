﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Models.SimpifiedModels
{
    public class UserUpdateModel
    {
#nullable enable
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime Birthday { get; set; }
    }
}
