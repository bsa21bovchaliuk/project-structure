﻿using Client.Enums;
using System;

namespace Client.Models.SimpifiedModels
{
    public class TaskCreateModel
    {
#nullable enable
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
    }
}
