﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Client.Models.ExtendedModels
{
    public class UserWithTasksModel
    {
        public UserModel User { get; set; }
        public IEnumerable<TaskModel> Tasks { get; set; }

        public override string ToString()
        {
            return $"{User}"
                + $"tasks:\n{Tasks.ToList().Aggregate(new StringBuilder(), (current, next) => current.Append($"{next.ToString($"{"",-Constants.Indent}")}\n")).ToString()}";
        }
    }
}
