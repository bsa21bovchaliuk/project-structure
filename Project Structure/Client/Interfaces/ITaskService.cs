﻿using Client.Models;
using Client.Models.ExtendedModels;
using Client.Models.SimpifiedModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Interfaces
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskModel>> GetTasksAsync();

        Task<TaskModel> GetTaskByIdAsync(int id);

        Task<TaskModel> CreateTaskAsync(TaskCreateModel taskCreateModel);

        Task<TaskModel> UpdateTaskAsync(TaskUpdateModel taskUpdateModel);

        Task DeleteTaskByIdAsync(int id);

        Task<IEnumerable<TaskModel>> GetTasksPerformedByUser(int id);

        Task<IEnumerable<TaskFinishedModel>> GetFinishedTasksByUser(int id);

        Task<IEnumerable<UserWithTasksModel>> GetTasksGroupedByUser();
    }
}
