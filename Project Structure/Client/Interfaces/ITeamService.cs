﻿using Client.Models;
using Client.Models.SimpifiedModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Interfaces
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamModel>> GetTeamsAsync();

        Task<TeamModel> GetTeamByIdAsync(int id);

        Task<TeamModel> CreateTeamAsync(TeamCreateModel teamCreateModel);

        Task<TeamModel> UpdateTeamAsync(TeamUpdateModel teamUpdateModel);

        Task DeleteTeamByIdAsync(int id);
    }
}
