﻿using Client.Interfaces;
using Client.Models;
using Client.Models.ExtendedModels;
using Client.Models.SimpifiedModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client.Services
{
    public class TaskService : ITaskService
    {
        private HttpClient _client;

        public TaskService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<TaskModel>> GetTasksAsync()
        {
            var tasks = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Tasks");
            return JsonConvert.DeserializeObject<IEnumerable<Models.TaskModel>>(tasks);
        }

        public async Task<TaskModel> GetTaskByIdAsync(int id)
        {
            var response = await _client.GetAsync($"{Constants.BaseUrl}/api/Tasks/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TaskModel>();
        }

        public async Task<TaskModel> CreateTaskAsync(TaskCreateModel taskCreateModel)
        {
            var response = await _client.PostAsJsonAsync($"{Constants.BaseUrl}/api/Tasks", taskCreateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TaskModel>();
        }

        public async Task<TaskModel> UpdateTaskAsync(TaskUpdateModel taskUpdateModel)
        {
            var response = await _client.PutAsJsonAsync($"{Constants.BaseUrl}/api/Tasks", taskUpdateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TaskModel>();
        }

        public async Task DeleteTaskByIdAsync(int id)
        {
            var response = await _client.DeleteAsync($"{Constants.BaseUrl}/api/Tasks/{ id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
        }

        public async Task<IEnumerable<TaskModel>> GetTasksPerformedByUser(int id)
        {
            var response = await _client.GetAsync($"{Constants.BaseUrl}/api/Tasks/performedByUser/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<IEnumerable<TaskModel>>();
        }

        public async Task<IEnumerable<TaskFinishedModel>> GetFinishedTasksByUser(int id)
        {
            var response = await _client.GetAsync($"{Constants.BaseUrl}/api/Tasks/finishedByUser/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<IEnumerable<TaskFinishedModel>>();
        }

        public async Task<IEnumerable<UserWithTasksModel>> GetTasksGroupedByUser()
        {
            var tasks = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Tasks/groupedByUser");
            return JsonConvert.DeserializeObject<IEnumerable<UserWithTasksModel>>(tasks);
        }
    }
}
