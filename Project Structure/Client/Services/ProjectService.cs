﻿using Client.Interfaces;
using Client.Models;
using Client.Models.ExtendedModels;
using Client.Models.SimpifiedModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;


namespace Client.Services
{
    public class ProjectService : IProjectService
    {
        private HttpClient _client;

        public ProjectService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<ProjectModel>> GetProjectsAsync()
        {
            var projects = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Projects");
            return JsonConvert.DeserializeObject<IEnumerable<ProjectModel>>(projects);
        }

        public async Task<ProjectModel> GetProjectByIdAsync(int id)
        {
            var response = await _client.GetAsync($"{Constants.BaseUrl}/api/Projects/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<ProjectModel>();
        }

        public async Task<ProjectModel> CreateProjectAsync(ProjectCreateModel projectCreateModel)
        {
            var response = await _client.PostAsJsonAsync($"{Constants.BaseUrl}/api/Projects", projectCreateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<ProjectModel>();
        }

        public async Task<ProjectModel> UpdateProjectAsync(ProjectUpdateModel projectUpdateModel)
        {
            var response = await _client.PutAsJsonAsync($"{Constants.BaseUrl}/api/Projects", projectUpdateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<ProjectModel>();
        }

        public async Task DeleteProjectByIdAsync(int id)
        {
            var response = await _client.DeleteAsync($"{Constants.BaseUrl}/api/Projects/{ id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
        }

        public async Task<string> GetProjects_TaskCountByUser(int id)
        {
            var response = await _client.GetAsync($"{Constants.BaseUrl}/api/Projects/Projects_TaskCount/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<IEnumerable<ProjectDetailedInfoModel>> GetProjectsInfo()
        {
            var projects = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Projects/DetailedInfo");
            return JsonConvert.DeserializeObject<IEnumerable<ProjectDetailedInfoModel>>(projects);
        }
    }
}
