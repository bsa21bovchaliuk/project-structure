﻿using Client.Interfaces;
using Client.Models;
using Client.Models.ExtendedModels;
using Client.Models.SimpifiedModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client.Services
{
    public class UserService : IUserService
    {
        private HttpClient _client;

        public UserService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<UserModel>> GetUsersAsync()
        {
            var users = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Users");
            return JsonConvert.DeserializeObject<IEnumerable<UserModel>>(users);
        }

        public async Task<UserModel> GetUserByIdAsync(int id)
        {
            var response = await _client.GetAsync($"{Constants.BaseUrl}/api/Users/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<UserModel>();
        }

        public async Task<UserModel> CreateUserAsync(UserCreateModel userCreateModel)
        {
            var response = await _client.PostAsJsonAsync($"{Constants.BaseUrl}/api/Users", userCreateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<UserModel>();
        }

        public async Task<UserModel> UpdateUserAsync(UserUpdateModel userUpdateModel)
        {
            var response = await _client.PutAsJsonAsync($"{Constants.BaseUrl}/api/Users", userUpdateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<UserModel>();
        }

        public async Task DeleteUserByIdAsync(int id)
        {
            var response = await _client.DeleteAsync($"{Constants.BaseUrl}/api/Users/{ id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
        }

        public async Task<IEnumerable<TeamWithUsersModel>> GetUsersGroupedByTeam()
        {
            var users = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Users/GroupedByTeam");
            return JsonConvert.DeserializeObject<IEnumerable<TeamWithUsersModel>>(users);
        }

        public async Task<UserDetailedInfoModel> GetUserInfo(int id)
        {
            var response = await _client.GetAsync($"{Constants.BaseUrl}/api/Users/DetailedInfo/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<UserDetailedInfoModel>();
        }
    }
}
