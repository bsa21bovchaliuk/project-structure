﻿using Client.Interfaces;
using Client.Models;
using Client.Models.SimpifiedModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client.Services
{
    public class TeamService : ITeamService
    {
        private HttpClient _client;

        public TeamService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<TeamModel>> GetTeamsAsync()
        {
            var teams = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Teams");
            return JsonConvert.DeserializeObject<IEnumerable<TeamModel>>(teams);
        }

        public async Task<TeamModel> GetTeamByIdAsync(int id)
        {
            var response = await _client.GetAsync($"{Constants.BaseUrl}/api/Teams/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TeamModel>();
        }

        public async Task<TeamModel> CreateTeamAsync(TeamCreateModel teamCreateModel)
        {
            var response = await _client.PostAsJsonAsync($"{Constants.BaseUrl}/api/Teams", teamCreateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TeamModel>();
        }

        public async Task<TeamModel> UpdateTeamAsync(TeamUpdateModel teamUpdateModel)
        {
            var response = await _client.PutAsJsonAsync($"{Constants.BaseUrl}/api/Teams", teamUpdateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TeamModel>();
        }

        public async Task DeleteTeamByIdAsync(int id)
        {
            var response = await _client.DeleteAsync($"{Constants.BaseUrl}/api/Teams/{ id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
        }
    }
}
