﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public static class Constants
    {
        public const string BaseUrl = "http://localhost:1982";
        public const int MaxNameLength = 45;
        public const int MinAge = 10;
        public const int Indent = 16;
    }
}
