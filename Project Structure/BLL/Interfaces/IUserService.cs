﻿using BLL.DTOs.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDTO>> GetAllAsync();

        Task<UserDTO> GetByIdAsync(int id);

        Task<UserDTO> CreateAsync(UserCreateDTO teamDTO);

        Task<UserDTO> UpdateAsync(UserUpdateDTO teamDTO);

        Task DeleteByIdAsync(int id);

        Task<IEnumerable<TeamWithUsersDTO>> GetUsersGroupedByTeam();

        Task<UserDetailedInfoDTO> GetUserInfo(int Id);
    }
}
