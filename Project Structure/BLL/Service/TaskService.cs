﻿using AutoMapper;
using BLL.DTOs.Task;
using BLL.DTOs.User;
using BLL.Interfaces;
using BLL.Services.Abstract;
using BLL.UnitOfWork.Interfaces;
using DAL.Entities;
using DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TaskService : BaseService, ITaskService
    {
        private readonly IRepository<TaskEntity> _repository;
        private readonly IProjectService _projectService;
        private readonly IUserService _userService;

        public TaskService(IUnitOfWork unitOfWork, IMapper mapper, IProjectService projectService, IUserService userService)
            : base(unitOfWork, mapper)
        {
            _repository = _unitOfWork.Set<TaskEntity>();
            _projectService = projectService;
            _userService = userService;
        }

        public async Task<TaskDTO> CreateAsync(TaskCreateDTO taskDTO)
        {
            var task = _mapper.Map<TaskEntity>(taskDTO);

            var project = await _projectService.GetByIdAsync(taskDTO.ProjectId);
            if (project == null)
                throw new ArgumentException("Project not found!");

            var performer = await _userService.GetByIdAsync(taskDTO.PerformerId);
            if (performer == null)
                throw new ArgumentException("Performer not found!");

            task.CreatedAt = DateTime.Now;
            await _repository.CreateAsync(task);

            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TaskDTO>(task);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var isDeleted = await _repository.DeleteByIdAsync(id);
            if (!isDeleted)
                throw new ArgumentException("Task not found!");
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<TaskDTO>> GetAllAsync()
        {
            var tasks = await _repository.GetAllAsync();
            return tasks.Select(task => _mapper.Map<TaskDTO>(task));
        }

        public async Task<TaskDTO> GetByIdAsync(int id)
        {
            var task = await _repository.GetByIdAsync(id);
            if (task == null)
                throw new ArgumentException("Task not found!");
            return _mapper.Map<TaskDTO>(task);
        }

        public async Task<TaskDTO> UpdateAsync(TaskUpdateDTO taskDTO)
        {
            var task = await _repository.GetByIdAsync(taskDTO.Id);

            if (task == null)
                throw new ArgumentException("Task not found!");

            var project = await _projectService.GetByIdAsync(taskDTO.ProjectId);
            if (project == null)
                throw new ArgumentException("Project not found!");

            var performer = await _userService.GetByIdAsync(taskDTO.PerformerId);
            if (performer == null)
                throw new ArgumentException("Performer not found!");

            var taskUpdated = _mapper.Map<TaskEntity>(taskDTO);
            taskUpdated.CreatedAt = task.CreatedAt;

            await _repository.UpdateAsync(taskUpdated);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TaskDTO>(taskUpdated);
        }

        public async Task<IEnumerable<TaskDTO>> GetTasksPerformedByUser(int userId)
        {
            var user = await _userService.GetByIdAsync(userId);
            if (user == null)
                throw new ArgumentException("User not found!");

            var tasks = await _repository.GetAllAsync();
            return tasks.Where(task => task.PerformerId == userId && task.Name.Length < Constants.MaxNameLength)
                         .Select(task => _mapper.Map<TaskDTO>(task));
        }

        public async Task<IEnumerable<TaskFinishedDTO>> GetFinishedTasksByUser(int userId)
        {
            var user = await _userService.GetByIdAsync(userId);
            if (user == null)
                throw new ArgumentException("User not found!");

            var tasks = await _repository.GetAllAsync();
            return tasks.Where(task => task.PerformerId == userId && task.State == TaskState.Finished && task.FinishedAt.Year == DateTime.Now.Year)
                                .Select(task => _mapper.Map<TaskFinishedDTO>(task));

        }

        public async Task<IEnumerable<UserWithTasksDTO>> GetTasksGroupedByUser()
        {
            var tasks = await _repository.GetAllAsync();

            return tasks.OrderByDescending(task => task.Name.Length)
                               .GroupBy(task => task.Performer)
                               .OrderBy(grp => grp.Key.FirstName)
                               .Select(grp => new UserWithTasksDTO
                               {
                                   User = _mapper.Map<UserDTO>(grp.Key),
                                   Tasks = grp.Select(task => _mapper.Map<TaskDTO>(task))
                               });
        }
    }
}
