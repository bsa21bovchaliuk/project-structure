﻿using AutoMapper;
using BLL.DTOs.Team;
using BLL.Interfaces;
using BLL.Services.Abstract;
using BLL.UnitOfWork.Interfaces;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TeamService : BaseService, ITeamService
    {
        private readonly IRepository<TeamEntity> _repository;

        public TeamService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _repository = _unitOfWork.Set<TeamEntity>();
        }

        public async Task<TeamDTO> CreateAsync(TeamCreateDTO teamDTO)
        {
            var team = _mapper.Map<TeamEntity>(teamDTO);
            team.CreatedAt = DateTime.Now;
            await _repository.CreateAsync(team);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(team);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var isDeleted = await _repository.DeleteByIdAsync(id);
            if (!isDeleted)
                throw new ArgumentException("Team not found!");
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<TeamDTO>> GetAllAsync()
        {
            var teams = await _repository.GetAllAsync();
            return teams.Select(team => _mapper.Map<TeamDTO>(team));
        }

        public async Task<TeamDTO> GetByIdAsync(int id)
        {
            var team = await _repository.GetByIdAsync(id);
            if (team == null)
                throw new ArgumentException("Team not found!");
            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<TeamDTO> UpdateAsync(TeamUpdateDTO teamDTO)
        {
            var team = await _repository.GetByIdAsync(teamDTO.Id);

            if (team == null)
                throw new ArgumentException("Team not found!");

            var teamUpdated = _mapper.Map<TeamEntity>(teamDTO);
            teamUpdated.CreatedAt = team.CreatedAt;

            await _repository.UpdateAsync(teamUpdated);

            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(teamUpdated);
        }
    }
}
