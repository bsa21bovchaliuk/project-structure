﻿using AutoMapper;
using BLL.DTOs.Project;
using BLL.DTOs.Task;
using BLL.Interfaces;
using BLL.Services.Abstract;
using BLL.UnitOfWork.Interfaces;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        private readonly IRepository<ProjectEntity> _repository;
        private readonly IUserService _userService;
        private readonly ITeamService _teamService;

        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper, IUserService userService, ITeamService teamService)
            : base(unitOfWork, mapper)
        {
            _repository = _unitOfWork.Set<ProjectEntity>();
            _userService = userService;
            _teamService = teamService;
        }

        public async Task<ProjectDTO> CreateAsync(ProjectCreateDTO projectDTO)
        {
            var project = _mapper.Map<ProjectEntity>(projectDTO);

            var author = await _userService.GetByIdAsync(projectDTO.AuthorId);
            if (author == null)
                throw new ArgumentException("Author not found!");

            var team = await _teamService.GetByIdAsync(projectDTO.TeamId);
            if (team == null)
                throw new ArgumentException("Team not found!");

            project.CreatedAt = DateTime.Now;
            await _repository.CreateAsync(project);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var project = await _repository.GetByIdAsync(id);
            if (project == null)
                throw new ArgumentException("Project not found!");

            await _repository.DeleteByIdAsync(id);

            var taskRepository = _unitOfWork.Set<TaskEntity>();
            var tasks = (await taskRepository.GetAllAsync())
                .Where(task => task.ProjectId == id)
                .ToList();
            foreach (var task in tasks)
            {
                await taskRepository.DeleteByIdAsync(task.Id);
            }

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllAsync()
        {
            var projects = await _repository.GetAllAsync();
            return projects.Select(project => _mapper.Map<ProjectDTO>(project));
        }

        public async Task<ProjectDTO> GetByIdAsync(int id)
        {
            var project = await _repository.GetByIdAsync(id);
            if (project == null)
                throw new ArgumentException("Project not found!");
            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task<ProjectDTO> UpdateAsync(ProjectUpdateDTO projectDTO)
        {
            var project = await _repository.GetByIdAsync(projectDTO.Id);
            if (project == null)
                throw new ArgumentException("Project not found!");

            var author = await _userService.GetByIdAsync(projectDTO.AuthorId);
            if (author == null)
                throw new ArgumentException("Author not found!");

            var team = await _teamService.GetByIdAsync(projectDTO.TeamId);
            if (team == null)
                throw new ArgumentException("Team not found!");

            var projectUpdated = _mapper.Map<ProjectEntity>(projectDTO);
            projectUpdated.CreatedAt = project.CreatedAt;

            await _repository.UpdateAsync(projectUpdated);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<ProjectDTO>(projectUpdated);
        }

        public async Task<Dictionary<ProjectDTO, int>> GetProjects_TaskCountByUser(int userId)
        {
            var user = await _userService.GetByIdAsync(userId);
            if (user == null)
                throw new ArgumentException("User not found!");

            var projects = await _repository.GetAllAsync();

            return projects.Where(project => project.AuthorId == userId)
                            .ToDictionary(project => _mapper.Map<ProjectDTO>(project), project => project.Tasks.Count());
        }

        public async Task<IEnumerable<ProjectDetailedInfoDTO>> GetProjectsInfo()
        {
            var tasks = await _repository.GetAllAsync();
            var users = await _userService.GetAllAsync();

            return tasks.Select(project => new ProjectDetailedInfoDTO
            {
                Project = _mapper.Map<ProjectDTO>(project),
                LongestTask = _mapper.Map<TaskDTO>(project.Tasks.OrderByDescending(task => task.Description).FirstOrDefault()),
                ShortestTask = _mapper.Map<TaskDTO>(project.Tasks.OrderBy(task => task.Name).FirstOrDefault()),
                TeamMembersCount = (project.Description.Length > Constants.MinDescriptionLength || project.Tasks.Count() < Constants.MaxTasksCount) ?
                                                                users.Count(user => user.TeamId == project.Team.Id) : default
            });
        }
    }
}
