﻿using AutoMapper;
using BLL.DTOs.Project;
using BLL.DTOs.Task;
using BLL.DTOs.User;
using BLL.Interfaces;
using BLL.Services.Abstract;
using BLL.UnitOfWork.Interfaces;
using DAL.Entities;
using DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IRepository<UserEntity> _repository;
        private readonly ITeamService _teamService;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, ITeamService teamService) : base(unitOfWork, mapper)
        {
            _repository = _unitOfWork.Set<UserEntity>();
            _teamService = teamService;
        }

        public async Task<UserDTO> CreateAsync(UserCreateDTO userDTO)
        {
            var user = _mapper.Map<UserEntity>(userDTO);

            var team = await _teamService.GetByIdAsync(userDTO.TeamId ?? default);
            if (team == null)
                throw new ArgumentException("Team not found!");


            user.RegisteredAt = DateTime.Now;
            await _repository.CreateAsync(user);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<UserDTO>(user);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var isDeleted = await _repository.DeleteByIdAsync(id);
            if (!isDeleted)
                throw new ArgumentException("User not found!");
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var users = await _repository.GetAllAsync();
            return users.Select(user => _mapper.Map<UserDTO>(user));
        }

        public async Task<UserDTO> GetByIdAsync(int id)
        {
            var team = await _repository.GetByIdAsync(id);
            if (team == null)
                throw new ArgumentException("User not found!");
            return _mapper.Map<UserDTO>(team);
        }

        public async Task<UserDTO> UpdateAsync(UserUpdateDTO userDTO)
        {
            var user = await _repository.GetByIdAsync(userDTO.Id);
            if (user == null)
                throw new ArgumentException("User not found!");


            var team = await _teamService.GetByIdAsync(userDTO.TeamId ?? default);
            if (team == null)
                throw new ArgumentException("Team not found!");


            var userUpdated = _mapper.Map<UserEntity>(userDTO);
            userUpdated.RegisteredAt = user.RegisteredAt;

            await _repository.UpdateAsync(userUpdated);

            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<UserDTO>(userUpdated);
        }

        public async Task<IEnumerable<TeamWithUsersDTO>> GetUsersGroupedByTeam()
        {
            var teams = await _teamService.GetAllAsync();
            var users = await _repository.GetAllAsync();

            return teams.GroupJoin(users.Where(user => user.TeamId.HasValue).OrderByDescending(user => user.RegisteredAt),
                                    team => team.Id,
                                    user => user.TeamId.Value,
                                    (team, users) => new TeamWithUsersDTO
                                    {
                                        Id = team.Id,
                                        Name = team.Name,
                                        Users = users.Select(user => _mapper.Map<UserDTO>(user))
                                    })
                                .Where(item => item.Users.All(user => DateTime.Now.Year - user.Birthday.Year > Constants.MinAge) && item.Users.Count() > 0);
        }

        public async Task<UserDetailedInfoDTO> GetUserInfo(int userId)
        {
            var users = await _repository.GetAllAsync();

            var projectRepository = _unitOfWork.Set<ProjectEntity>();
            var _projects = await projectRepository.GetAllAsync();

            var taskRepository = _unitOfWork.Set<TaskEntity>();
            var _tasks = await taskRepository.GetAllAsync();

            var user = users.Where(user => user.Id == userId)
                            .Select(user =>
                            {
                                var lastProject = _projects.Where(project => project.AuthorId == user.Id)
                                                           .OrderByDescending(project => project.CreatedAt)
                                                           .FirstOrDefault();
                                var tasks = _tasks.Where(task => task.PerformerId == user.Id);
                                return new UserDetailedInfoDTO
                                {
                                    User = _mapper.Map<UserDTO>(user),
                                    LastProject = _mapper.Map<ProjectDTO>(lastProject),
                                    LastProjectTasksCount = lastProject != null ? lastProject.Tasks.Count() : default,
                                    UnfinishedTasksCount = tasks.Count(task => task.State != TaskState.Finished),
                                    LongestTask = _mapper.Map<TaskDTO>(tasks.OrderByDescending(task => task.FinishedAt - task.CreatedAt).FirstOrDefault())
                                };
                            })
                            .FirstOrDefault();

            if (user == null)
                throw new ArgumentException("User not found!");

            return user;
        }
    }
}
