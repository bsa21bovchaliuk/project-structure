﻿using BLL.DTOs.User;
using System.Collections.Generic;

namespace BLL.DTOs.Task
{
    public class UserWithTasksDTO
    {
        public UserDTO User { get; set; }
        public IEnumerable<TaskDTO> Tasks { get; set; }
    }
}
