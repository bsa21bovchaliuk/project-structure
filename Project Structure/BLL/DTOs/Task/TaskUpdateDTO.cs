﻿using DAL.Enums;
using System;

namespace BLL.DTOs.Task
{
    public class TaskUpdateDTO
    {
#nullable enable
        public int Id { get; set; }
        public int ProjectId { get; set; }

        public int PerformerId { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

        public DateTime FinishedAt { get; set; }

        public TaskState State { get; set; }
    }
}
