﻿using System;

namespace BLL.DTOs.User
{
    public class UserCreateDTO
    {
#nullable enable
        public int? TeamId { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public string? Email { get; set; }

        public DateTime Birthday { get; set; }
    }
}
