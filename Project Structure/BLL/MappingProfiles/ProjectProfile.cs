﻿using AutoMapper;
using BLL.DTOs.Project;
using DAL.Entities;

namespace BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectEntity, ProjectDTO>();

            CreateMap<ProjectCreateDTO, ProjectEntity>();
            CreateMap<ProjectUpdateDTO, ProjectEntity>();
        }
    }
}
