﻿using AutoMapper;
using BLL.DTOs.Task;
using DAL.Entities;

namespace BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskEntity, TaskDTO>();
            CreateMap<TaskEntity, TaskFinishedDTO>();

            CreateMap<TaskCreateDTO, TaskEntity>();
            CreateMap<TaskUpdateDTO, TaskEntity>();
        }
    }
}
