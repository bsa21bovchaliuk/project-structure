﻿using DAL.Entities.Abstract;
using System.Threading.Tasks;

namespace BLL.UnitOfWork.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<TEntity> Set<TEntity>() where TEntity : BaseEntity;

        void SaveChanges();

        Task SaveChangesAsync();
    }
}
