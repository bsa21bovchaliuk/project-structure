﻿using Bogus;
using DAL.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Context
{
    public sealed class Context
    {
        private const string filename = "context.json";

        public Context()
        {
            Teams = new List<TeamEntity>();
            Users = new List<UserEntity>();
            Projects = new List<ProjectEntity>();
            Tasks = new List<TaskEntity>();
        }

        public Context(RandomDataGenerator randomDataGenerator)
        {
            if (File.Exists(filename))
            {
                LoadContextAsync();
            }
            else
            {
                Seed(randomDataGenerator);
            }
            LoadData();
        }

        public List<TaskEntity> Tasks { get; private set; }
        public List<TeamEntity> Teams { get; private set; }
        public List<UserEntity> Users { get; private set; }
        public List<ProjectEntity> Projects { get; private set; }

        private void Seed(RandomDataGenerator randomDataGenerator)
        {
            Randomizer.Seed = new Random(2020);

            Teams = randomDataGenerator.GenerateRandomTeams().ToList();
            Users = randomDataGenerator.GenerateRandomUsers(Teams).ToList();
            Projects = randomDataGenerator.GenerateRandomProjects(Users, Teams).ToList();
            Tasks = randomDataGenerator.GenerateRandomTasks(Users, Projects).ToList();
        }


        public void SaveChanges()
        {
            LoadData();
            using StreamWriter stream = File.CreateText(filename);
            var serializer = new JsonSerializer();
            serializer.Serialize(stream, this);
        }

        public async Task SaveChangesAsync()
        {
            await Task.Factory.StartNew(() => SaveChanges());
        }

        public void LoadContextAsync()
        {
            using StreamReader stream = File.OpenText(filename);
            var serializer = new JsonSerializer();
            var ctx = (Context)serializer.Deserialize(stream, typeof(Context));
            Teams = ctx.Teams;
            Users = ctx.Users;
            Projects = ctx.Projects;
            Tasks = ctx.Tasks;
        }

        private void LoadData()
        {
            Tasks = Tasks.Join(Users,
                    task => task.PerformerId,
                    user => user.Id,
                    (task, user) =>
                    {
                        task.Performer = user;
                        return task;
                    })
                .ToList();

            Users = Users.Join(Teams,
                    user => user.TeamId,
                    team => team.Id,
                    (user, team) =>
                    {
                        user.Team = team;
                        return user;
                    })
                .ToList();

            Projects = Projects.Join(Users,
                    project => project.AuthorId,
                    user => user.Id,
                    (project, user) =>
                    {
                        project.Author = user;
                        return project;
                    })
                .Join(Teams,
                    project => project.TeamId,
                    team => team.Id,
                    (project, team) =>
                    {
                        project.Team = team;
                        return project;
                    })
                .GroupJoin(Tasks,
                    project => project.Id,
                    task => task.ProjectId,
                    (project, tasks) =>
                    {
                        project.Tasks.AddRange(tasks);
                        return project;
                    })
                .ToList();
        }


        public List<TEntity> Set<TEntity>() where TEntity : class
        {
            return (List<TEntity>)typeof(Context).GetProperties()
                    .Where(pi => pi.PropertyType == typeof(List<TEntity>))
                    .Select(pi => pi.GetValue(this))
                    .FirstOrDefault();
        }
    }
}
