﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public sealed class TeamEntity : BaseEntity
    {
#nullable enable
        public string? Name { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
