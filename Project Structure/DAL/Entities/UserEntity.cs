﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public sealed class UserEntity : BaseEntity
    {
#nullable enable
        public int? TeamId { get; set; }

        public TeamEntity? Team { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public string? Email { get; set; }

        public DateTime Birthday { get; set; }

        public DateTime RegisteredAt { get; set; }
    }
}
