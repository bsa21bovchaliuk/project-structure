﻿using DAL.Entities.Abstract;
using DAL.Enums;
using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public sealed class TaskEntity : BaseEntity
    {
#nullable enable
        public int ProjectId { get; set; }
        //public ProjectEntity? Project { get; set; }

        public int PerformerId { get; set; }
        public UserEntity? Performer { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime FinishedAt { get; set; }

        public TaskState State { get; set; }
    }
}
