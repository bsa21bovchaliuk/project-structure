﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTOs.User;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<UserDTO>>> Get()
        {
            return Ok(await _userService.GetAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetById(int id)
        {
            try
            {
                return Ok(await _userService.GetByIdAsync(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Post([FromBody] UserCreateDTO userDTO)
        {
            try
            {
                var user = await _userService.CreateAsync(userDTO);
                return Created($"api/Users/{user.Id}", user);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult<UserDTO>> Put([FromBody] UserUpdateDTO userDTO)
        {
            try
            {
                return Ok(await _userService.UpdateAsync(userDTO));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {
            try
            {
                await _userService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("GroupedByTeam")]
        public async Task<ActionResult<IEnumerable<TeamWithUsersDTO>>> GetUsersGroupedByTeam()
        {
            return Ok(await _userService.GetUsersGroupedByTeam());
        }

        [HttpGet("DetailedInfo/{id}")]
        public async Task<ActionResult<UserDetailedInfoDTO>> GetUserInfo(int id)
        {
            try
            {
                return Ok(await _userService.GetUserInfo(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
